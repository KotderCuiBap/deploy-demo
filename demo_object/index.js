//array [] ~luuw nhiều thông tin cùng loại ~danh sách sv,dánh sách sđt

// object { }~ mô tả các thông tin khác nhau của 1 đối tượng (nhân vật game,nhân vien trong công ty)

//key:value

var sv1 = {
  name: "alice",
  age: 3,
  gmail: "alice@gmail.com",
};
console.log("sv1: ", sv1);

//dùng key để lấy value
/**
 * lấy giá trị
 * array=>index
 * object=>key
 */

var cat1 = {
  //property
  name: "miu",
  age: "1",
  //method
  speak: function () {
    console.log("Meo meo", this.name, sv2.name);
  },
  child: {
    name: "miu con",
    age: "0.5",
    speak: function () {
      console.log("meo meo ", this.name);
      return this.name;
    },
  },
  sdt: ["093123", "093456"],
};
console.log("suun ", cat1.child.speak());

var sv2 = {
  name: "bob", //key:value
  age: 3,
  gmail: "bob@gmail.com",
};
console.log("ten sv2", sv2.name);
//update value
cat1.name = "mun";
console.log("cat1: ", cat1);
cat1.speak();
