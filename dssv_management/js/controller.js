function renderDSSV(dssv) {
  console.log("dssv aun: ", dssv);
  var contentHTML = ``;
  for (var i = 0; i < dssv.length; i++) {
    var item = dssv[i];
    var contentTr = `
      <tr>
          <td>${item.ma}</td>
          <td>${item.ten}</td>
          <td>${item.email}</td>
          <td>0</td>
          <td>
              <button class="btn btn-danger" onclick="xoaSV(${item.ma})">
                Xóa
                </button>
                <button class="btn btn-danger" onclick="suaSV(${item.ma})">
                Sửa
                </button>
          </td>
      </tr>
      `;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function layThongTinTuForm() {
  //lấy thông tin từ form
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value;
  var ly = document.getElementById("txtDiemLy").value;
  var hoa = document.getElementById("txtDiemHoa").value;

  var sv = {
    ma: ma,
    ten: ten,
    email: email,
    matKhau: matKhau,
    toan: toan,
    ly: ly,
    hoa: hoa,
  };
  console.log({
    ma,
    ten,
    email,
    matKhau,
    toan,
    ly,
    hoa,
  });
  return sv;
}
